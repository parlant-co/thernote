APP   := "thernote"
IMAGE := "registry.lukasrieder.com/$(APP):latest"

.ONESHELL: build push deploy

release: build push deploy

build:
	docker build -t $(IMAGE) .

push:
	scp docker-compose.yml root@weserworld.lukasrieder.com:$(APP).yml
	docker push $(IMAGE)

deploy:
	ssh root@weserworld.lukasrieder.com "docker-compose -f $(APP).yml pull"
	-ssh root@weserworld.lukasrieder.com "docker-compose -f $(APP).yml down"
	ssh root@weserworld.lukasrieder.com "docker-compose -f $(APP).yml up -d"

