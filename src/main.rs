extern crate sodiumoxide;
extern crate base64;
extern crate hex;
extern crate salvo;
extern crate chrono;
extern crate tokio_schedule;

// Encryption
use sodiumoxide::crypto::stream::xsalsa20::{self, Key, Nonce};
use sodiumoxide::crypto::hash;
// Storage
use std::fs::{self, File};
use std::io::Write;
// Server
use salvo::prelude::*;
use salvo::extra::serve_static::{self, DirHandler};
use salvo::extra::size_limiter::max_size;
// Background Sweeper
use chrono::{Utc};
use tokio::spawn;
use tokio_schedule::{every, Job};


#[handler]
async fn create_note(req: &mut Request, res: &mut Response) {
    if let Some(plaintext) = req.form::<String>("plaintext").await {
        if let Some(keystring) = note("tmp", &plaintext) {
            res.render(keystring);
        } else {
            res.set_status_error(StatusError::internal_server_error());
        }
    } else {
        res.set_status_error(StatusError::not_found());
    };
}

#[handler]
async fn read_secret(req: &mut Request, res: &mut Response) {
    if let Some(keystring) = req.form::<String>("key").await {
        if let Some(plaintext) = readnote("tmp", &keystring) {
            res.render(plaintext);
        } else {
            res.set_status_error(StatusError::not_found());
        }
    } else {
        res.set_status_error(StatusError::not_found());
    }
}

#[tokio::main]
async fn main() {
    let hourly = every(1).hour().in_timezone(&Utc).perform(|| async {
        let mut entries = match fs::read_dir("tmp") {
            Ok(entries) => entries,
            Err(_) => return
        };
        while let Some(Ok(entry)) = entries.next() {
            let path = entry.path();
            let age = match fs::metadata(&path) {
                Ok(metadata) => match metadata.modified() {
                    Ok(modified) => match modified.elapsed() {
                        Ok(elapsed) => elapsed.as_secs(),
                        Err(reason) => panic!("{:?}", reason)
                    },
                    Err(reason) => panic!("{:?}", reason)
                },
                Err(_) => return
            };
            if age > 3600 * 24 * 30 {
                match fs::remove_file(&path) {
                    Ok(_) => return,
                    Err(_) => return
                };
            }
        };
    });
    spawn(hourly);

    let router = Router::new()
        .push(Router::with_path("notes").hoop(max_size(1024 * 10)).post(create_note))
        .push(Router::with_path("secrets").hoop(max_size(1024 * 10)).post(read_secret))
        .push(
            Router::with_path("<**path>").get(DirHandler::width_options(vec!["static"], serve_static::Options {
                dot_files: false,
                listing: true,
                defaults: vec!["index.html".to_owned()]
            }))
        );
    Server::new(TcpListener::bind("0.0.0.0:8080")).serve(router).await;
}

fn note(folder: &str, plaintext: &str) -> Option<String> {
    let key = xsalsa20::gen_key();
    let nonce = xsalsa20::gen_nonce();
    let ciphertext = xsalsa20::stream_xor(plaintext.as_bytes(), &nonce, &key);
    let keynonce:Vec<u8> = [key.as_ref(), nonce.as_ref()].concat();
    let digest = hash::hash(&keynonce);
    let filename = hex::encode(&digest);

    match fs::create_dir_all(folder) {
        Ok(_) => match File::create(format!("{folder}/{filename}.bin")) {
            Ok(mut file) => match file.write_all(&ciphertext) {
                Ok(_) => match file.sync_all() {
                    Ok(_) => return Some(base64::encode(&keynonce).into()),
                    Err(_) => return None
                },
                Err(_) => return None
            },
            Err(_) => return None
        },
        Err(_) => return None
    };
}

fn readnote(folder: &str, b64keynonce: &str) -> Option<String> {
    let keynonce = match base64::decode(b64keynonce) {
        Ok(keynonce) => keynonce,
        Err(_) => return None
    };
    // validate keynonce
    if keynonce.len() != 56 {
        return None;
    }
    let digest = hash::hash(&keynonce);
    let filename = hex::encode(&digest);
    let path = format!("{folder}/{filename}.bin");
    // check age
    let age = match fs::metadata(&path) {
        Ok(metadata) => match metadata.modified() {
            Ok(modified) => match modified.elapsed() {
                Ok(elapsed) => elapsed.as_secs(),
                Err(reason) => panic!("{:?}", reason)
            },
            Err(reason) => panic!("{:?}", reason)
        },
        Err(_) => return None
    };
    if age > 3600 * 24 * 30 {
        fs::remove_file(&path).ok()?;
        return None;
    }
    // decrypt
    let ciphertext = match std::fs::read(&path) {
        Ok(contents) => contents,
        Err(_) => return None
    };
    fs::remove_file(&path).ok()?;
    let (key_b, nonce_b) = keynonce.split_at(32);
    let key = Key::from_slice(&key_b)?;
    let nonce = Nonce::from_slice(&nonce_b)?;
    let plaintext = xsalsa20::stream_xor(&ciphertext, &nonce, &key);
    match std::str::from_utf8(&plaintext) {
        Ok(v) => return Some(v.into()),
        Err(_) => return None
    }
}

