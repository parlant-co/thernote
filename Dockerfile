ARG PACKAGE_NAME=thernote
ARG TARGET=x86_64-unknown-linux-musl

FROM registry.gitlab.com/rust_musl_docker/image:stable-latest AS buildstage
ARG TARGET

WORKDIR /app

RUN rustup target add ${TARGET}

COPY . /app

RUN cargo build --release --target ${TARGET}
RUN cargo install --target ${TARGET} --path .

FROM scratch AS target

ARG PACKAGE_NAME
ARG TARGET

COPY --from=buildstage /app/target/${TARGET}/release/${PACKAGE_NAME} /${PACKAGE_NAME}
COPY --from=buildstage /app/static /static

CMD ["/thernote"]

